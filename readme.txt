Installation
To install the project and its dependencies, follow these steps:

Clone the repository to your local machine.
git clone https://gitlab.com/bartek.gutowski/pytest.git

Navigate to the project directory.
cd project

Create a virtual environment (optional but recommended).
python -m venv venv

Activate the virtual environment.
On Windows:
venv\Scripts\activate

On macOS and Linux:
source venv/bin/activate

Install the project dependencies.

pip install -r requirements.txt


