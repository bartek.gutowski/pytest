import pytest
from Solutions_main.product_param import calculate_product


# create test cases based on different sets of input parameters
@pytest.mark.parametrize("num3", [7, 8, 9], ids=["seven", "eight", "nine"])
@pytest.mark.parametrize("num2", [4, 5, 6], ids=["four", "five", "six"])
@pytest.mark.parametrize("num1", [1, 2, 3], ids=["one", "two", "three"])

# test function takes 3 parameters and assert output of calculate_product
def test_product(num1, num2, num3):
    assert calculate_product([num1, num2, num3]) == num1 * num2 * num3