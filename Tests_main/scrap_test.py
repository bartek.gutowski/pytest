import pytest
from Solutions_main.scrap import search_renault
import os

type_of_car = "laguna" #example: clio, megane, laguna
@pytest.fixture
def search_result():
    return search_renault(type_of_car)
# check if csv file exists

# check if header has correct columns
check_header = ["Title", "Year", "Mileage", "Price"]
@pytest.mark.parametrize("checked_name", check_header)
def test_header_has_column(checked_name):
    df = search_renault(checked_name)
    assert df.columns.tolist() == check_header
# Check if the year has 4 characters
@pytest.mark.parametrize("search_query", [type_of_car])
def test_year_has_four_characters(search_query):
    df = search_renault(search_query)
    assert df["Year"].str.len().eq(4).all()

# Check if the price ends with "zł"
@pytest.mark.parametrize("search_query", [type_of_car])
def test_price_ends_with_zl(search_query):
    df = search_renault(search_query)
    assert df["Price"].str.endswith("zł").all()

def test_csv_file_exists():
    assert os.path.exists("renault.csv")
