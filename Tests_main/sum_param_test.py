import pytest
from Solutions_main.sum_param import calculateSum

test_data = [
    pytest.param(1, 2, 3, id="1+2=3"),
    pytest.param(2, 2, 4, id="2+2=4"),
    pytest.param(3, 3, 6, id="3+3=6"),
] + [pytest.param(x//2, x - x//2, x, id=f"{x//2}+{x-x//2}={x}") for x in range(0, 101, 10)]


@pytest.mark.parametrize("num1, num2, result", test_data)
def test_addition(num1, num2, result):
    assert calculateSum([num1, num2]) == result
