import pytest
from Solutions_main.valid_date import is_valid_date

@pytest.mark.parametrize("year, month, day, expected",
[
    (1, 1, 1, True),
    (0, 0, 0, False),
    (2021, 4, 31, False),
    (2023, 2, 29, False),
    (2023, 12, 31, True),
    (2023, 13, 1, False)
])
# function called for each tuple, and the input arguments are passed to the is_valid_date
def test_is_valid_date(year, month, day, expected):
    assert is_valid_date(year, month, day) == expected
