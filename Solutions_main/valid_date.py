# Solution to problem 21 (valid date) from Python Programming Exercises Gently Explained by Al Sweigart
import calendar
# function that  checks if the date is valid
def is_valid_date(year, month, day):
    if (year < 0 or month < 1 or month > 12 or day < 1 or day > 31):
        return False
    if (month == 2):
        if (calendar.isleap(year) and day == 29):
            return True
        elif (day > 28):
            return False
    elif (month in [4, 6, 9, 11] and day > 30):
        return False
    elif (day > 31):
        return False
    return True

