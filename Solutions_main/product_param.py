# Solution to problem 13 (calculate product) from Python Programming Exercises Gently Explained by Al Sweigart
# takes a list of numbers as an argument and returns the product of all the numbers in the list
def calculate_product(numbers):
# if the list passed to calculateProduct() is empty, the function returns 1
    result = 1
    for number in numbers:
        result *= number

    return result

