# Program will search for Renault cars
# on olx.pl website
# and save it to CSV file that contains
# title, year, mileage and price

import requests
from bs4 import BeautifulSoup
import pandas as pd
import os
def search_renault(search_query):
    # build URL

    url = f"https://www.olx.pl/d/motoryzacja/samochody/renault/pomorskie/q-{search_query}/"

    # make request to URL
    response = requests.get(url)

    # parse HTML using BeautifulSoup
    soup = BeautifulSoup(response.content, "html.parser")
    elements = soup.select('div.css-t4djs0')

    # find all listings on page
    listings = soup.find_all("div", class_="css-u2ayx9")

    # create empty list to hold data
    data = []

    # iterate through listings and extract information
    for listing, element in zip(listings, elements):
        # extract title and price
        title = listing.find("h6", class_="css-16v5mdi er34gjf0").text.strip()[:30]
        price_element = listing.find("p", class_="css-10b0gli er34gjf0")
        price = price_element.text.strip() + " zł" if price_element else "N/A"
        price = price.replace(" złdo negocjacji", "")
        price = price.replace(" zł zł", " zł")
        year, mileage = element.text.split("-") if "-" in element.text else ("", "")
        year = year.strip()
        mileage = mileage.strip().replace(" ", "")[:-2] + " km"
        # add information to data list
        data.append([title, year, mileage, price])

    # create pandas DataFrame from data
    df = pd.DataFrame(data, columns=["Title", "Year", "Mileage", "Price"])
    df.to_csv("renault.csv", index= False)
    return df

