# Solution to problem 13 (calculate sum) from Python Programming Exercises Gently Explained by Al Sweigart
# takes a list of numbers as input and returns their sum
def calculateSum(numbers):
    #If the list passed to calculateSum() is empty, the function returns 0.
    result = 0
    for number in numbers:
        result += number

    return result










